package com.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerMatch {

    private static final String TAG = JSONResponseHandlerMatch.class.getSimpleName();

    public Team team;


    public JSONResponseHandlerMatch(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            if (reader.peek() != JsonToken.NULL)
                readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        String home = "";
        String away = "";
        String label = "";
        int HomeScore = -1;
        int AwayScore = -1;
        int id = 0;
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                if (reader.peek() == JsonToken.NULL)
                    reader.skipValue();
                String name = reader.nextName();

                if (nb==0) {
                    if (name.equals("strHomeTeam")) {
                        home = reader.nextString();
                    } else if (name.equals("strAwayTeam")) {
                        away = reader.nextString();
                    } else if (name.equals("strEvent")) {
                        label = reader.nextString();
                    } else if (name.equals("idEvent")) {
                        id = reader.nextInt();
                    } else if (name.equals("intHomeScore")) {
                        if (reader.peek() != JsonToken.NULL)
                            HomeScore = reader.nextInt();
                    } else if (name.equals("intAwayScore")) {
                        if (reader.peek() != JsonToken.NULL)
                            AwayScore = reader.nextInt();
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();

        team.setLastEvent(new Match(id, label, home, away, HomeScore, AwayScore));
    }

}
