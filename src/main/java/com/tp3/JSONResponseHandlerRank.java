package com.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerRank {

    private static final String TAG = JSONResponseHandlerRank.class.getSimpleName();

    public Team team;


    public JSONResponseHandlerRank(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            if (reader.peek() == JsonToken.NULL) reader.skipValue();
            else {
                String name = reader.nextName();
                if (name.equals("table")) {
                    readArrayTeams(reader);
                } else {
                    reader.skipValue();
                }
            }
        }
        reader.endObject();
    }



    private void readArrayTeams(JsonReader reader) throws IOException {
        String nameValue = "";
        int score = -1;
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()){
                String name = reader.nextName();
                if (name.equals("name")) {
                    nameValue = reader.nextString();
                    if (nameValue.equals(team.getName())) team.setRanking(nb + 1);
                } else if (name.equals("total")){
                    team.setTotalPoints(reader.nextInt());
                }else reader.skipValue();
                nb++;
                }
            reader.endObject();
        }
        reader.endArray();
    }

}
