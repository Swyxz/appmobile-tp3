package com.tp3;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SportDbHelper dbHelper = new SportDbHelper(this);
        dbHelper.populate();

        ListView lstView = (ListView) findViewById(R.id.lstView);
        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(v.getContext(), TeamActivity.class);
                /*Bundle b = new Bundle();
                intent.putExtra("Team", b);*/
                Cursor z = (Cursor) parent.getItemAtPosition(position);
                String t =z.getString(z.getColumnIndex(SportDbHelper.COLUMN_TEAM_NAME));
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                Cursor current = db.query(dbHelper.TABLE_NAME, new String[]{dbHelper._ID, dbHelper.COLUMN_TEAM_NAME, dbHelper.COLUMN_TEAM_ID,
                                dbHelper.COLUMN_LEAGUE_NAME, dbHelper.COLUMN_LEAGUE_ID, dbHelper.COLUMN_STADIUM, dbHelper.COLUMN_STADIUM_LOCATION,
                                dbHelper.COLUMN_TEAM_BADGE, dbHelper.COLUMN_TOTAL_POINTS, dbHelper.COLUMN_RANKING, dbHelper.COLUMN_LAST_MATCH_ID,
                                dbHelper.COLUMN_LAST_MATCH_LABEL, dbHelper.COLUMN_LAST_MATCH_HOME_TEAM, dbHelper.COLUMN_LAST_MATCH_AWAY_TEAM,
                                dbHelper.COLUMN_LAST_MATCH_SCORE_HOME, dbHelper.COLUMN_LAST_MATCH_SCORE_AWAY, dbHelper.COLUMN_LAST_UPDATE},
                        dbHelper.COLUMN_TEAM_NAME+"=?", new String[]{t}, null, null, null);
                current.moveToFirst();
                Team a = dbHelper.cursorToTeam(current);
                intent.putExtra(Team.TAG, a);
                db.close();
                startActivityForResult(intent, 2);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NewTeamActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Team team = (Team) data.getParcelableExtra(Team.TAG);
                SportDbHelper dbHelper = new SportDbHelper(this);
                dbHelper.addTeam(team);
            }
        }
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){
                Team team = (Team) data.getParcelableExtra(Team.TAG);
                SportDbHelper dbHelper = new SportDbHelper(this);
                dbHelper.updateTeam(team);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        SportDbHelper dbHelper = new SportDbHelper(this);
        final ListView lstView = findViewById(R.id.lstView);
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams(),
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.
                        COLUMN_LEAGUE_NAME },
                new int[] { android.R.id.text1, android.R.id.text2});
        lstView.setAdapter(adapter);
    }
}
